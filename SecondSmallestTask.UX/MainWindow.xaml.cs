﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SecondSmallestTask.UX
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnProcess_Click(object sender, RoutedEventArgs e)
        {
            int[] array = StringToIntList(txtArrayArea.Text).ToArray();
            txtArrayArea.Text = string.Join(",", array.Select(x => x.ToString()));

            lblResult.Content = SecondSmallest.GetSecondSmallest(array).ToString();

        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {

            int arraySize;
            if (!int.TryParse(txtArraySize.Text, out arraySize)) MessageBox.Show("Please ensure, you're writing a proper integer");

            int[] array = IntArrayGenerator.GenerateIntArray(arraySize);

            lblResult.Content = "";
            txtArrayArea.Text = string.Join(",", array.Select(x => x.ToString()));
        }

        private static IEnumerable<int> StringToIntList(string str)
        {
            if (String.IsNullOrEmpty(str))
                yield break;

            foreach (var s in str.Split(','))
            {
                int num;
                if (int.TryParse(s, out num))
                    yield return num;
            }
        }


    }
}
