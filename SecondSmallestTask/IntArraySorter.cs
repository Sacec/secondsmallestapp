﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondSmallestTask
{
    public class IntArraySorter
    {
        static public void SortIntArray(int[] array)
        {

            sort(array, 0, array.Length - 1);

        }

        private static void sort(int[] array, int left, int right)
        {
            if (left < right)
            {
                int pivot = array[(left + right) / 2];

                int partitionPointer = processPivot(array, left, right, pivot);

                sort(array, left, partitionPointer - 1);
                sort(array, partitionPointer, right);
            }
            return;

        }

        private static int processPivot(int[] array, int left, int right, int pivot)
        {
            while (left <= right)
            {
                while (array[left] < pivot)
                {
                    left++;
                }
                while (array[right] > pivot)
                {
                    right--;
                }

                if (left <= right)
                {
                    int temp = array[right];
                    array[right] = array[left];
                    array[left] = temp;
                    left++;
                    right--;
                }
            }
            return left;
        }

    }
}
