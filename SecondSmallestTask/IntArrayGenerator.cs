﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondSmallestTask
{
    public class IntArrayGenerator
    {
        static Random _r = new Random();

        static public int[] GenerateIntArray(int size)
        {
            int[] intArray = new int[size];

            for (int i = 0; i < size; i++)
            {
                intArray[i] = _r.Next();
            }

            return intArray;
        }
    }
}
