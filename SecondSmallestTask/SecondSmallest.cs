﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondSmallestTask
{
    public class SecondSmallest
    {
        public static int GetSecondSmallest(int[] array)
        {
            if (array == null) throw new ArgumentNullException("array");

            //initial state of output for cases: a)array has 2 or less elements & b)array contains just duplicated values
            int output = 0;

            if (array.Length > 2)
            {
                IntArraySorter.SortIntArray(array);

                int[] smallestsArray = GetSmallestIntegersInArray(array, 2);

                if(smallestsArray.Length >= 2)
                {
                    output = smallestsArray[1];
                }                
            }
            
            return output;
        }

        private static int[] GetSmallestIntegersInArray(int[] array, int lenght)
        {
            HashSet<int> avoidDuplicates = new HashSet<int>();
            int indx = 0;

            while(avoidDuplicates.Count() < lenght && indx < array.Length)
            {
                avoidDuplicates.Add(array[indx]);
                indx++;
            }

            return avoidDuplicates.ToArray();
        }
    }
}
