﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace SecondSmallestTask.UnitTest
{
    [TestClass]
    public class SecondSmallestTaskUnitTest
    {
        [TestMethod]
        public void AbleToFindSecondSmallestInt()
        {
            int[] array = new int[]
            {
                695376480,1083668846,197456027,1993759024,3,90,667630841,1826892315,279996887,1426206774,491521980,1958012538,1368467373,14678952,1372212002,532926560,908728593,652210617,1281702752,496871711,1894264311,508313032,1898572367,4778943,12873604,591557609,333561650,453742378,1863294329,932037256,1856651549,1365904579,1547711928,1954639980,952367840,952047066,75201167,270934622,1557989895,655158317,331739602,4710917,1769821049,878968344,285184178,146565834,1447106423,1595695982,1813521469,1481446534,777348610,2025656008,174611290,306683348,2099790543,1085187005,944287760,1078889903,184582423,1402201415,334069191,1373149937,564186205,494169518,782354078,592107959,1968239092,207522619,419844162,1728363141,1903035603,637793971,1241704369,626544435,165132109,1889553394,885975630,1019604023,1867078412,2013791417,1291934833,885349315,787704556,381847795,154688646,1978479188,1191293289,1241028580,2002333084,2014664482,7759306,1143794911,86352199,155788480,321089126,1106073312,1588008359,1275651531,96614266,1840559866
            };

            int secondSmallest = SecondSmallest.GetSecondSmallest(array);

            Assert.AreEqual(90, secondSmallest);
        }

        [TestMethod]
        public void ReturnSecondSmallestNoneDuplicatedInteger()
        {
            int[] array = new int[] { 1, 34, 2, 1, 55, 1, 91, 102, 1, 76 };

            int secondSmallest = SecondSmallest.GetSecondSmallest(array);

            Assert.AreEqual(2, secondSmallest);
        }

        [TestMethod]
        public void Return0ForTwoOrLessElementsInArray()
        {
            int[] array1 = new int[]{ 1,2 };
            int[] array2 = new int[] { 1 };
            int[] array3 = new int[0];
            int[] array4 = new int[] { };

            Assert.AreEqual(0, SecondSmallest.GetSecondSmallest(array1));
            Assert.AreEqual(0, SecondSmallest.GetSecondSmallest(array2));
            Assert.AreEqual(0, SecondSmallest.GetSecondSmallest(array3));
            Assert.AreEqual(0, SecondSmallest.GetSecondSmallest(array4));
        }

        [TestMethod]
        public void Returns0ForArrayOfDuplicates()
        {
            int[] array = new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

            Assert.AreEqual(0, SecondSmallest.GetSecondSmallest(array));
        }

        [TestMethod]
        public void ThrowArgumentNullExceptionForNullArray()
        {
            int[] array = null;

            try
            {
                SecondSmallest.GetSecondSmallest(array);
            }
            catch (ArgumentNullException e)
            {
                Assert.AreEqual("array", e.ParamName);
            }
        }

    }

    [TestClass]
    public class HelperClassesUnitTests
    {
        [TestMethod]
        public void ArrayISWellSortedWithTaskSortMethod()
        {
            int[] array = new int[] { 8, 3, 5, 4, 2, 1, 9, 10, 6, 7 };
            int[] arrayInOrder = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            IntArraySorter.SortIntArray(array);

            Assert.AreEqual("1,2,3,4,5,6,7,8,9,10", string.Join(",", array.Select(x => x.ToString()).ToArray()));
        }

        [TestMethod]
        public void ArrayIsProperlyGeneratedByTask()
        {
            int[] array = IntArrayGenerator.GenerateIntArray(34);

            Assert.IsTrue(array.Length == 34);
        }

    }
}
